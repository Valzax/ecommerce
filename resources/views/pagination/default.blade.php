@if ($paginator->lastPage() > 1)
    <div>
        <ul class="pagination pagination-sm justify-content-end">
            <li class="{{ ($paginator->currentPage() == 1) ? ' disabled' : '' }}">
                <a href="{{ $paginator->url(1) }}" class="{{ ($paginator->currentPage() == 1) ? ' disabled' : '' }}">Anterior</a>
            </li>
            @for  ($i = 1; $i <= $paginator->lastPage(); $i++)
                <li class="{{ ($paginator->currentPage() == $i) ? ' active' : '' }}">
                    <a href="{{ $paginator->url($i) }}">{{ $i }}</a>
                </li>
            @endfor
            @if($paginator->currentPage() == $paginator->lastPage())
                <li class="disabled"><a href="#">Siguiente</a></li>
            @else
                <li class="{{ ($paginator->currentPage() == $paginator->lastPage()) ? ' disabled' : '' }}">
                    <a href="{{ $paginator->url($paginator->currentPage()+1) }}" class="{{ ($paginator->currentPage() == $paginator->lastPage()) ? ' disabled' : '' }}">Siguiente</a>
                </li>
            @endif
        </ul>
    </div>
@endif