    <!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Cafe CL</title>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <style>
        .containerWeb{
            width: 50%;
            margin: 0 auto;
        }
        .sticky-top{
            position: fixed;
            top: 0;
            width: 100%;
        }
    </style>
</head>
<body>
<header>
    <!------------             NAVBAR STICKY      ------------->

    <div class="sticky-top">
        <nav class="navbar navbar-expand-md navbar-dark bg-dark">
            <div class="container">
                <a class="navbar-brand" href="index.html"><!--<img alt="logo" src={{ asset('assets/logo.png') }}>--></a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse " id="navbarsExampleDefault">
                    <ul class="navbar-nav m-auto">
                        <li class="nav-item">
                            <a class="nav-link" href="{{ url('/') }}">Inicio</a>
                        </li>
                        <li class="nav-item active">
                            <a class="nav-link" href="{{ url('/cart') }}">Categorias <span class="sr-only">(current)</span></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{ url('/cart') }}">Carrito</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{ url('/cart') }}">Contacto</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{ url('/cart') }}">Ofertas</a>
                        </li>
                    </ul>

                    <form class="form-inline my-2 my-lg-0">
                        <div class="input-group input-group-sm">
                            <input type="text" class="form-control" aria-label="Small" aria-describedby="inputGroup-sizing-md" placeholder="Search...">
                            <div class="input-group-append">
                                <button type="button" class="btn btn-secondary btn-number">
                                    <i class="fa fa-search"></i>
                                </button>
                            </div>
                        </div>
                        <div class="btn btn-success btn-sm ml-3">
                            @if (Route::has('login'))
                                <div class="top-right links">
                                    @auth
                                        <a href="{{ url('/') }}">
                                            <!------------cart logo------------->
                                            <i class="fa fa-shopping-cart"></i>Home
                                            <span onclass="badge badge-light"></span>
                                        </a>
                                    @else
                                        <a href="{{ route('login') }}">Login</a>

                                        @if (Route::has('register'))
                                            <a href="{{ route('register') }}">Register</a>
                                        @endif
                                    @endauth
                                </div>
                            @endif
                        </div>
                    </form>
                </div>
            </div>
        </nav>
    </div>
    <!------------Descripccion de pagina  opcional------------->
    <div class="jumbotron jumbotron-fluid">
        <div class="container">
            <h1 class="display-4">Fluid jumbotron</h1>
            <p class="lead">This is a modified jumbotron that occupies the entire horizontal space of its parent.</p>
        </div>
    </div>
</header>
<div class="container">
    <div class="row">
        <div class="col-12 col-sm-3">
            <?php
                $connect = mysqli_connect('localhost','root','','ecommerce');
                $query = 'SELECT * FROM products order by id ASC';
                $result = mysqli_query($connect,$query);
                if($result){
                    if(mysqli_num_rows($result)>0){
                        while($product = mysqli_fetch_assoc($result));
                    }
                }
            ?>
        </div>
    </div>
</div>
</body>
</html>