<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Cafe CL</title>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link href="font-awesome/{{asset( 'fonts/backend_fonts/css/font-awesome.css')}}" rel="stylesheet" />
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700,800' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.8/css/solid.css">


    <style>
        body{
            background: url({{ asset('assets/background.png') }}) no-repeat center center fixed;
            background-size: cover;
        }

        .main-section{
            margin:0 auto;
            margin-top:25%;
            padding: 0;
        }

        .modal-content{
            background-color: #3b4652;
            opacity: .85;
            padding: 0 20px;
            box-shadow: 0px 0px 3px #848484;
        }
        .user-img{
            margin-top: -50px;
            margin-bottom: 35px;
        }

        .user-img img{
            width: 100px;
            height: 100px;
            box-shadow: 0px 0px 3px #848484;
            border-radius: 50%;
        }

        .form-group input{
            height: 42px;
            font-size: 18px;
            border:0;
            padding-left: 54px;
            border-radius: 5px;
        }

        .form-group::before{
                        position: absolute;
            left: 28px;
            font-size: 22px;
            padding-top:4px;
        }

        button{
            width: 60%;
            margin: 5px 0 25px;
        }

        .forgot{
            padding: 5px 0;
        }

        .forgot a{
            color: white;
        }

    </style>
</head>
<body>
<div class="modal-dialog text-center">
    <div class="col-sm-8 main-section">
        <div class="modal-content">
            <div class="col-12 user-img">
                <img src="{{ asset('assets/logo.png') }}"/>
            </div>
            <form class="col-12"  method="get">
                <div class="form-group" id="mail">
                    <input type="text" class="form-control" placeholder="Email@dominio.com" name="email"/>
                </div>
                <div class="form-group" id="pass">
                    <input type="password" class="form-control" placeholder="Contrasena" name="password"/>
                </div>
                <button type="submit" class="btn btn-primary"><i class="fas fa-sign-in-alt"></i>  Ingresar </button>
            </form>
            <div class="col-12 forgot">
                <a href="#">Recordar contrase&ntilde;a?</a>
            </div>
            <div class="col-12 forgot">
                <a href="#">Registrarse</a>
            </div>

        </div>
    </div>
</div>
</body>
</html>
